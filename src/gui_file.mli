(** File utilities for GUI *)

(** Open a window to chose a json file and load it in the game_state *)
val chose_game : unit -> unit

(** Recreate the game_state with the file "filename" if is set
 * in client_state *)
val reload_game : unit -> unit
